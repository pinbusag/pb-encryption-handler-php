<?php

namespace EncryptionHandler\Encryption;

class Encryption 
{
    protected $key;
    protected $method;
    protected $data;
    protected $iv;

    function __construct($password = '', $initialVector = '') {
        $this->key = $password;
        $this->iv = $initialVector;
    }

    public function encrypt($data){
        $encrypted_data = openssl_encrypt($data, 'aes-256-cbc', $this->key, OPENSSL_RAW_DATA, $this->iv);
        return base64_encode($encrypted_data);
    }
    
    public function decrypt($data){
        $value = base64_decode($data);
        $data = openssl_decrypt($value, 'aes-256-cbc', $this->key, OPENSSL_RAW_DATA, $this->iv);
        return $data;
    }
}
